package com.pnc.studentservice.Repo;

import com.pnc.studentservice.Model.Student;
import org.springframework.stereotype.Repository;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
@Repository
public class RepolImpl implements Repol {
    public static  List<Student> students = new ArrayList<>();
    public RepolImpl() {
    }
    @Override
    public List<Student> getstudents() {
        if(students.size()==0){return null;}
        return students;
    }
    @Override
    public Student getstudentid(int id) {
        for (Student s : students) {
            if (s.getId() == id) {
                return s; }
        }
        return null;
    }
    @Override
    public List<Student> getstudentname( String name) {
        List<Student> studentsnames = new ArrayList<>();
        for(Student ss: students) {
            if (ss.getName().startsWith(name)) {
                studentsnames.add(ss);
            }
        }

        if(studentsnames.size()==0){return null;}
        else{
        return studentsnames;}
    }

    @Override
    public String addStudent(int id, String name) {

        if(students.size()==0){ students.add(new Student(id, name));
        return "added";}
        for(Student s:students) {
            if (s.getId() == id) {return "student with this id is already present , change id";}
            else {
                students.add(new Student(id, name));
                return "success";
            }
        }
        return null;
    }
    @Override
    public Student updateStudent(Student student) {
        System.out.println("i am in update");
        int id;
        id=student.getId();
        for(Student s :students){
            if(s.getId()==id){
                s.setName(student.getName());
                return s;
            }

        }
        return null;
    }
    @Override
    public String  deleteStudent(int id) {
        for(Student s : students){
            if(s.getId()==id){students.remove(s);return ("successfully removed student with id ---"+s.getId()+"the size of the list is---"+students.size());}
        }
        return "failed to find the student";
    }
}
