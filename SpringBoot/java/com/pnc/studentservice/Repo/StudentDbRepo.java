package com.pnc.studentservice.Repo;


import com.pnc.studentservice.Model.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentDbRepo extends JpaRepository<StudentEntity,Integer> {




}
