package com.pnc.studentservice.Repo;


import com.pnc.studentservice.Model.Student;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;


public interface Repol {

    public List<Student> getstudents();
    public Student getstudentid(int id);
    public List<Student> getstudentname(String name);
    public String deleteStudent(int id);
    public String addStudent(int id,String name);
    public Student updateStudent(Student student);


}

