package com.pnc.studentservice.Service;




import com.pnc.studentservice.Model.Student;
import com.pnc.studentservice.Model.StudentEntity;
import com.pnc.studentservice.Repo.Repol;
import com.pnc.studentservice.Repo.StudentDbRepo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@org.springframework.stereotype.Service
public class ServiceImpl implements Service {

    @Autowired
    Repol repo;

    @Autowired
    StudentDbRepo studentDbRepo;

StudentEntity s1;



    @Override
    public List<Student> getstudents() {
        //return repo.getstudents() ;
        return studentDbRepo.findAll().stream().map(s->new Student(s.getId(),s.getName())).collect(Collectors.toList());
    }

    @Override
    public Student getstudentid(int id) {
       // return repo.getstudentid(id) ;
        s1=studentDbRepo.findById(id).orElse(null);
        if(s1==null){return null;}
        else
        return new Student( s1.getId(),s1.getName());
    }

    @Override
    public List<Student> getstudentname(String name) {
       // return repo.getstudentname(name) ;

        List<Student> s2 =studentDbRepo.findAll().stream().filter(s->s.getName().startsWith(name)).map(s->new Student(s.getId(),s.getName())).collect(Collectors.toList());
        if(s2==null){return null;}
        else
            return s2;
    }

    @Override
    public String addStudent(int id, String name) {
        //return repo.addStudent(id,name);
        StudentEntity s3 = new StudentEntity(name);
        studentDbRepo.save(s3);
        return "successfull added"+s3.getId();
    }


    @Override
    public String deletestudent(int id) {
        //return repo.deleteStudent(id);
        if(studentDbRepo.findById(id).orElseThrow(null)!=null){
            studentDbRepo.delete(studentDbRepo.getOne(id));
            return "deleted";
        }
        else return "please enter a valid id";



    }



    @Override
    public Student updateStudent(Student student) {
        //return repo.updateStudent(student);
        if(studentDbRepo.findById(student.getId()).orElse(null)!=null){

            //studentDbRepo.save(studentDbRepo.getOne(student.getId()).setName(student.getName()));
            StudentEntity s4=studentDbRepo.getOne(student.getId());
            s4.setName(student.getName());

            studentDbRepo.save(s4);

            return new Student (s4.getId(),s4.getName());
        }
        else return null;


    }
}


