package com.pnc.studentservice.Service;



import com.pnc.studentservice.Model.Student;

import java.util.List;

public interface Service {

    public List<Student> getstudents();
    public Student getstudentid(int id);
    public List<Student> getstudentname(String name);
    public String deletestudent(int id);
    public String addStudent(int id, String name);
    public Student updateStudent(Student student);

}
