package com.pnc.studentservice.controller;


import com.pnc.studentservice.Model.Student;
import com.pnc.studentservice.Service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    Service service;

    @GetMapping
    @ResponseBody
    public List<Student> getStudent()
    {
        return service.getstudents();
    }

    @GetMapping("/{id}")
    @ResponseBody
    public Student getStudentid(@PathVariable("id") int id)
    {
        return service.getstudentid(id);
    }

    @GetMapping("/name/{name}")
    @ResponseBody
    public List<Student> getStudentname(@PathVariable("name") String name)
    {
        return service.getstudentname(name);
    }

    @PostMapping ("/add/{id}/{name}")
    public String addStudent(@PathVariable("id") int id,@PathVariable("name") String name){return service.addStudent(id,name);}

    @PutMapping ("/update")
    @ResponseBody
    public Student updateStudent(@RequestBody Student student){return service.updateStudent(student);}

    @GetMapping("/delete/{id}")
    public String deleteStudent(@PathVariable("id") int id){return service.deletestudent(id);}


}
