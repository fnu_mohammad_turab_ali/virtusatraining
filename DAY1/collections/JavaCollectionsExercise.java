import java.util.*;
import java.util.Map.Entry;

public class JavaCollectionsExercise {

    public static void main(String[] args) {

        //first question answer
        first f = new first();
        f.sort(4, 8);
        for (int i = 0; i < f.Arr.length; i++) {
            System.out.println(f.Arr[i]);
        }


        System.out.println("------------------------------------------------------------------------------------------------------------------------------");

        //second question answer
        second s = new second();
        s.add(1);
        s.add(2);
        s.add(3);
        s.search(4);

        System.out.println("------------------------------------------------------------------------------------------------------------------------------");

        //third question answer
        ArrayList<Integer> al = new ArrayList();
        ArrayList<Integer> al1;
        for(int i=0;i<10;i++){
            al.add(i*2);
        }
        for(int i=0;i<10;i++){
            System.out.println(al.get(i));
        }
        third t = new third();
        al1= t.copy(al);
        for(int i=0;i<10;i++){
            System.out.println(al1.get(i));
        }


        System.out.println("------------------------------------------------------------------------------------------------------------------------------");

        //fourth question answer
        LinkedList<Integer> ll4 = new LinkedList();
        ll4.add(1);
        ll4.add(2);
        ll4.add(3);
        fourth fo = new fourth();
        fo.reverse(ll4);

        System.out.println("------------------------------------------------------------------------------------------------------------------------------");

        //fifth question

        fifthnode node1 = new fifthnode(2);
        fifthnode head = node1;
        fifthnode node2 = new fifthnode(3);
        fifthnode node3 = new fifthnode(4);
        fifthnode node4 = new fifthnode(5);

        System.out.println("------------------------------------------------------------------------------------------------------------------------------");

        //sixth question

        LinkedList<Integer> ll6 = new LinkedList<>();
        ll6.add(1);
        ll6.add(2);ll6.add(3);ll6.add(4);ll6.add(5);ll6.add(6);

        Collections.shuffle(ll6);
        for(int i=0;i<ll6.size();i++){

            System.out.println(ll6.get(i));
        }
        System.out.println("------------------------------------------------------------------------------------------------------------------------------");

       //seventh question

        Set<Integer> hs = new HashSet<>();
        hs.add(10);
        hs.add(2);
        hs.add(3);
        hs.add(4);hs.add(200);hs.add(77);hs.add(86);hs.add(10000);hs.add(-1);hs.add(8);


        Iterator<Integer> hsiterator = hs.iterator();
        while(hsiterator.hasNext()){
            System.out.println(hsiterator.next());
        }
        Set<Integer> ts= new TreeSet<>(hs);
        Iterator<Integer> treesetiterator = ts.iterator();
        while(treesetiterator.hasNext()){

            System.out.println(treesetiterator.next());
        }
        System.out.println("------------------------------------------------------------------------------------------------------------------------------");

        //Eighth question

        TreeSet<Integer> ts1 = new TreeSet<>();
        ts1.add(1);ts1.add(2);ts1.add(3);ts1.add(4);ts1.add(5);ts1.add(6);ts1.add(7);ts1.add(8);
        TreeSet<Integer> ts2 = new TreeSet<>();
        ts2.add(1);ts2.add(2);ts2.add(3);ts2.add(4);ts2.add(5);ts2.add(6);ts2.add(7);ts2.add(8);
        System.out.println(ts1.equals(ts2));

        System.out.println("------------------------------------------------------------------------------------------------------------------------------");
        //ninth question

        TreeSet<Integer> ts3 = new TreeSet<>();
        ts3.add(1);ts3.add(2);ts3.add(3);ts3.add(4);ts3.add(5);ts3.add(6);ts3.add(7);ts3.add(8);
        ninth n = new ninth();
        n.lessthan(ts3,6);

        System.out.println("------------------------------------------------------------------------------------------------------------------------------");
        //tenth question

        PriorityQueue<Integer> pq = new PriorityQueue<Integer>();
        pq.add(43);pq.add(365);pq.add(234);pq.add(99899);pq.add(4534);pq.add(2323);pq.add(5776);pq.add(4);pq.add(4444);

        Object[] priorityqueuetoarray = pq.toArray();
        for(int i=0;i<priorityqueuetoarray.length;i++){
            System.out.println(priorityqueuetoarray[i]);
        }

        System.out.println("------------------------------------------------------------------------------------------------------------------------------");
        //eleventh question

        Map<Integer,Integer> map = new HashMap<Integer, Integer>() ;
        map.put(1,2);map.put(2,3);map.put(3,4);map.put(4,5);map.put(5,6);map.put(6,7);map.put(7,8);
        if(map.isEmpty()){
            System.out.println("map is empty");
        }
        System.out.println("map is not empty");

        System.out.println("------------------------------------------------------------------------------------------------------------------------------");
        //twelfth  question

        Map<Integer,Integer> maptenth = new HashMap<Integer, Integer>() ;
        maptenth.put(1,2);maptenth.put(2,3);maptenth.put(3,4);maptenth.put(4,5);maptenth.put(5,6);maptenth.put(6,7);maptenth.put(7,8);
        TreeMap<Integer,Integer> treemaptenth = new TreeMap<Integer, Integer>(maptenth) ;
        System.out.println(treemaptenth.tailMap(3,false));


        System.out.println("------------------------------------------------------------------------------------------------------------------------------");
        //thirteenth  question
        TreeMap<Integer,Integer> maptwelve = new TreeMap<Integer, Integer>() ;
        maptwelve.put(1,2);maptwelve.put(2,3);maptwelve.put(3,4);maptwelve.put(4,5);maptwelve.put(5,6);maptwelve.put(6,7);maptwelve.put(7,8);
        System.out.println( maptwelve.descendingKeySet());

        System.out.println("------------------------------------------------------------------------------------------------------------------------------");
        //fourteenth  question

        Map<Integer,Integer> mapthirteen = new TreeMap<Integer, Integer>(new sortusingcomparator()) ;
        mapthirteen.put(7,2);mapthirteen.put(4,3);mapthirteen.put(3,4);mapthirteen.put(2,5);mapthirteen.put(8,6);mapthirteen.put(6,7);mapthirteen.put(1,8);
        System.out.println(mapthirteen);
























    }



    }


class sortusingcomparator implements Comparator<Integer>{

    @Override
    public int compare(Integer i1,Integer i2){

        return i1.compareTo(i2);
    }


}

