public class Addition implements InterfaceFOrCalculator {

    private int op1;
    private int op2;

    public Addition(int op1, int op2) {
        this.op1 = op1;
        this.op2 = op2;
    }

    @Override
    public void operation() {
        System.out.println("sum is "  +  (op1+op2));
    }
}
