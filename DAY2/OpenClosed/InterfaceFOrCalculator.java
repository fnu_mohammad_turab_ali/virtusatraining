public interface InterfaceFOrCalculator {

    void operation();
    //if we donot have this method here then there is no way we can give each operation its own behaviour-
    //with in its class and then the calculator class would need ot handle it and when a new
    //operation is added then the calculator class needs to be changed which is in contrary to OCP
}
