public class OpenForExtension_ClosedForModification {

    public static void main(String[] args) {
        Addition add1= new Addition(1,2);
        Calculator firstoperation = new Calculator();
        firstoperation.calculate(add1);
    }

    //NOw suppose we wnat to add the subtraction operation we just have to extend the Interfaceorcalculator
    //and declare a class for substraction and we can see that not even a single class is being effected
    // because of this except the class containing the main .

}
