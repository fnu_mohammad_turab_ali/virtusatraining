public class Liskov {
    //A class can be replaced by its subclass in all scenarios

    Vehicle vehicle1 = new Bus();
    Vehicle vehicle2 = new Car();
    //As we can see we can Easily substitute a vehicle reference variable with objects of Car or Bus or any other subclass of Vehicle
}
