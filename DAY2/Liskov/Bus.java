public class Bus implements Vehicle {

    //no engine

    @Override
    public void startengine() {
        System.out.println("I donot have an engine please throw error");
    }

    @Override
    public void ignitionoff() {
        System.out.println("I donot have an engine ");
    }

    //now as the bus doesnt have a engine these two methods donot make sense even though bus is a vehicl.
    //so the solution is (doubt)
    //what to do if i donot want some of the methods defined in interface.
}
