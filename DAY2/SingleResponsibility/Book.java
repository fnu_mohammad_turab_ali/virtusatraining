public class Book {

    private String name;
    private String author;
    private String text;

    public Book(String name, String author, String text) {
        this.name = name;
        this.author = author;
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
    //Here all the methds that directly effect name,author and the text of a given book can be written
    public void find_aword_inthe_text(String word){
        if(text.contains(word)){
            System.out.println("the book has the given word");
        }
        else {
            System.out.println("sorry the word could not be found");
        }

    }

    //now if suppose you want to update the text you can have a method for it in here
    //similarly if you want to count the number of words in a text you can you can have -
    // the method in here
    //but now if we want to check how many copies of a given book are sold we need to have -
    //an other class for it(Doubt).
    //Now if I want to print the content of a book this class in itself doesn't have to do anything-
    //to do with it as printing a book to a console has nothing to do with the book in itself.
}
