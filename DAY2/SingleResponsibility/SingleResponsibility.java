public class SingleResponsibility {

    public static void main(String[] args) {


        Book book1= new Book("The Straner","Camus","Absurdism and Nihilism ");
        Book book2 = new Book("Code Complete","Steve","Good practices of coding");
        Printing print1= new Printing();
        Printing print2= new Printing();
        print1.PrintingToConsole(book1);
        print2.PrintingViaPrinter(book2);
    }
}
