public class WIndows98 {

    public Keyboard skboard;
    public Monitor monitor;

    //if we instantiate the above variable in the constructor like
    //this.skboard= new StandardKeyboard and the type being standardkeyboard our first problem arises
    //as fo reach new keyboard we have to make changes in this class so we have
    //used the interface . The second being if we hardcode the new Keyboard() int the constructor we
    // are injecting the dependency , instead of that we just give the reference type as constructor argument
    //and let the main class inject these dependencies for us.

    public WIndows98(Keyboard keyboard, Monitor monitor){

        this.skboard= keyboard;
        this.monitor= monitor;


    }
    //Here as the Keyboard is a interface it can take in any new implementation of Keyboard interface
    //be it standard or any other and also as we are not instantiating the objects in the constructor and
    //letting the main class so it for us and only as they are injected loose coupling is achieved.


}
