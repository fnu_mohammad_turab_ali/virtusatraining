package com.Question5;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Q5 {
    public static void main(String[] args) {

        ArrayList<Student> listofstudents = new ArrayList<>();
        Subject[] sub = Subject.values();
        HashMap<Subject , Integer> markss1 = new HashMap<>();
        HashMap<Subject , Integer> markss2 = new HashMap<>();
        HashMap<Subject , Integer> markss3 = new HashMap<>();
        HashMap<Subject , Integer> markss4 = new HashMap<>();
        HashMap<Subject , Integer> markss5 = new HashMap<>();
        HashMap<Subject , Integer> markss6 = new HashMap<>();
        HashMap<Subject , Integer> markss7 = new HashMap<>();
        HashMap<Subject , Integer> markss8 = new HashMap<>();


        //Marks of student1
        markss1.put(Subject.HISTORY,33);
        markss1.put(Subject.LANGUAGE,88);
        markss1.put(Subject.MATHEMATICS,100);
        markss1.put(Subject.SCIENCE,87);
        //create student objects one by one
        Student s1 = new Student("1",markss1);
        listofstudents.add(s1);
        //Marks of student1
        markss2.put(Subject.HISTORY,66);
        markss2.put(Subject.LANGUAGE,47);
        markss2.put(Subject.MATHEMATICS,46);
        markss2.put(Subject.SCIENCE,64);
        //create student objects one by one
        Student s2 = new Student("2",markss2);
        listofstudents.add(s2);
        //Marks of student1
        markss3.put(Subject.HISTORY,75);
        markss3.put(Subject.LANGUAGE,44);
        markss3.put(Subject.MATHEMATICS,34);
        markss3.put(Subject.SCIENCE,75);
        //create student objects one by one
        Student s3 = new Student("3",markss3);
        listofstudents.add(s3);
        //Marks of student1
        markss4.put(Subject.HISTORY,75);
        markss4.put(Subject.LANGUAGE,34);
        markss4.put(Subject.MATHEMATICS,75);
        markss4.put(Subject.SCIENCE,86);
        //create student objects one by one
        Student s4 = new Student("4",markss4);
        listofstudents.add(s4);
        //Marks of student1
        markss5.put(Subject.HISTORY,77);
        markss5.put(Subject.LANGUAGE,45);
        markss5.put(Subject.MATHEMATICS,75);
        markss5.put(Subject.SCIENCE,22);
        //create student objects one by one
        Student s5 = new Student("5",markss5);
        listofstudents.add(s5);
        //Marks of student1
        markss6.put(Subject.HISTORY,44);
        markss6.put(Subject.LANGUAGE,85);
        markss6.put(Subject.MATHEMATICS,22);
        markss6.put(Subject.SCIENCE,0);
        //create student objects one by one
        Student s6 = new Student("6",markss6);
        listofstudents.add(s6);
        //Marks of student1
        markss7.put(Subject.HISTORY,0);
        markss7.put(Subject.LANGUAGE,0);
        markss7.put(Subject.MATHEMATICS,0);
        markss7.put(Subject.SCIENCE,0);
        //create student objects one by one
        Student s7 = new Student("7",markss7);
        listofstudents.add(s7);
        //Marks of student1
        markss8.put(Subject.HISTORY,33);
        markss8.put(Subject.LANGUAGE,0);
        markss8.put(Subject.MATHEMATICS,100);
        markss8.put(Subject.SCIENCE,87);
        //create student objects one by one
        Student s8 = new Student("8",markss8);
        listofstudents.add(s8);

        Q5 answer = new Q5();
        Map<Subject, ArrayList<Student>> finalresult;
        finalresult = answer.getBestScores(listofstudents);

        System.out.println(finalresult.get(Subject.SCIENCE));
        System.out.println(finalresult.get(Subject.MATHEMATICS));
        System.out.println(finalresult.get(Subject.LANGUAGE));
        System.out.println(finalresult.get(Subject.HISTORY));





    }

    public Map<Subject, ArrayList<Student>> getBestScores(List<Student> students) {

        List<Student> stu=students;
        Subject[] sub1 = Subject.values();
        Map<Subject, ArrayList<Student>> answer = Map.of(Subject.HISTORY,new ArrayList<Student>(1),Subject.LANGUAGE,new ArrayList<Student>(1),Subject.MATHEMATICS,new ArrayList<Student>(1),Subject.SCIENCE,new ArrayList<Student>(1));
        int numberofstudents = answer.size();

        for(Subject s : sub1){
            for(int i=0;i<numberofstudents;i++){
                if(answer.get(s).isEmpty()){
                    answer.get(s).add(stu.get(i));
                }
                else if ((int)stu.get(i).report.get(s)>(int)answer.get(s).get(0).report.get(s)){
//                    answer.get(s).get(0).report.put(s,stu.get(i).report.get(s));

                    // you have to replace the student with the existing student
                    answer.get(s).set(0, stu.get(i));
                }
                else if((int)stu.get(i).report.get(s)==(int)answer.get(s).get(0).report.get(s)){
                    if(Integer.parseInt(stu.get(i).id)<Integer.parseInt(answer.get(s).get(0).id)){
                    }
                    else{
//                        answer.get(s).get(0).report.put(s,stu.get(i).report.get(s));
                        answer.get(s).set(0, stu.get(i));
                    }
                }


            }
        }






        return answer ;
    }
}
