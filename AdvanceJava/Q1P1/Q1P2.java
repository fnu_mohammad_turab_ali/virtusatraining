package com.Question1part2;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Q1P2 {

    public static void main(String[] args) {

        List<ArrayList<String>> listoflists = new ArrayList<ArrayList<String>>();
        List<String> output;
        ArrayList<String> list1 = new ArrayList<>();
        for(int i=0;i<4;i++){list1.add("I am in first list......");}
        listoflists.add(list1);
        ArrayList<String> list2 = new ArrayList<>();
        for(int i=0;i<4;i++){list2.add("I am in second list......");}
        listoflists.add(list2);
        ArrayList<String> list3 = new ArrayList<>();
        for(int i=0;i<4;i++){list3.add("I am in third list......");}
        listoflists.add(list3);
        ArrayList<String> list4 = new ArrayList<>();
        for(int i=0;i<4;i++){list4.add("I am in fourth list......");}
        listoflists.add(list4);

        Flatten flattened = new Flatten(listoflists);
        output=flattened.flattenlist();

        for(int i =0;i<output.size();i++){
          System.out.println(output.get(i));
        }


        System.out.println("...........................................");
        //using streams
        List<String> flatstreams = listoflists.stream().flatMap(x->x.stream()).collect(Collectors.toList());
        System.out.println(flatstreams);


    }
}
