package com.Question4;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Q4 {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("ali");list.add("ajit");list.add("sai");list.add("parth");list.add("bala");
        list.add("kavya");list.add("lewis");list.add("siva");

        Mapping map = new Mapping(list);

        Map<Integer,ArrayList<String>> output =  map.mapped();

        for(Integer keys : output.keySet()){

            System.out.println(keys +"..........."+output.get(keys));
        }

    }
}
