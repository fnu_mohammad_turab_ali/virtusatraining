package com.Question1;

import java.util.ArrayList;
import java.util.List;

public class Q1 {

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        double average;
        for(int i=0;i<10;i++){

            list.add(i);
        }



        Average avg = new Average(list);
        average=avg.avg();
        System.out.println("the average is "+average);
        System.out.println("if -1 is the out put please check the list ");

        //using streams

        List<Integer> liststream = new ArrayList<>();
        for(int i=0;i<10;i++){

            liststream.add(i);
        }

        System.out.println("..................................");

        double avgstream = liststream.stream().mapToDouble(x->x).average().orElse(0.0);

        System.out.println(avgstream);


    }
}
