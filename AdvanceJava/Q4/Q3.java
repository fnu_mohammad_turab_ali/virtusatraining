package com.Question3;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Q3 {

    public static void main(String[] args) {
        Map<String,String> map = new HashMap<>();
        String key;
        for (int i=0;i<10;i++){
            map.put("key"+i,"value"+i);
        }
        System.out.println("selec the key ");

        for(String keys : map.keySet()){

            System.out.println("KEY: "+keys+" = "+"VALUE: "+map.get(keys));

        }

        Scanner sc = new Scanner(System.in);
        key= sc.nextLine();


        Searching search = new Searching(map,key);
        String opt = search.search();
        System.out.println(opt);


        //with streams


        System.out.println("-----------------------------------");
        Set<Map.Entry<String,String>> entries = map.entrySet();

        entries.parallelStream().filter(m->m.getKey().equals("key3")).findFirst().orElse("f");


      //  entries.parallelStream().filter(m->m.getKey().equals("key")).forEach(e-> {if(e!=null){System.out.println(e.getValue());}
      //  else{
       //     System.out.println("not found");}
       // });


    }
}
