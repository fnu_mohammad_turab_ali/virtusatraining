package com.Question3;

import java.util.Map;

public class Searching {

    Map<String,String> map;
    String key;
    public Searching(Map<String,String> map,String key) {

        this.map=map;
        this.key=key;
    }

    public String search(){


        if(map.containsKey(key)){

            return map.get(key);
        }

        else{
            return "NOT_FOUND";
        }


    }
}
