package com.servlethttpsessionexample;




import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServletWIthSession")
public class LoginServletWIthSession extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final String userID = "admin";
	private final String password = "password";

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// get request parameters for userID and password
		String user = request.getParameter("user");
		String pwd = request.getParameter("pwd");
		
		if(userID.equals(user) && password.equals(pwd)){
			//Session cookies allow users to be recognized within a website
			//what we have to check is the new pages that we go to remember the past activities or not.
			HttpSession session = request.getSession();
			//what if existing session is present
			//what if the attribute is already present, do we set it again
			//It says we can use session in other resources or in future requests lets check
			session.setAttribute("user", "Pankaj");
			//setting session to expiry in 30 mins
			session.setMaxInactiveInterval(30*60);
			//why isnt the session being invalidated?
			Cookie userName = new Cookie("user", user);
			//why isn't a session id added in the cookie
			//in which step is session object added to response.
			userName.setMaxAge(30*60);
			response.addCookie(userName);
			
			//where are we attaching session object to response???
			response.sendRedirect("loginsucesswithsession.jsp");
		}else{
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/LoginWithSession.html");
			PrintWriter out= response.getWriter();
			out.println("<font color=red>Either user name or password is wrong.</font>");
			rd.include(request, response);
		}

	}

}

